@extends('layout.theme')

@section('title')
    List Profil Aktor
@endsection

@section('content')

<a href="/cast/create" class="btn btn-secondary mb-3">Tambah Aktor</a>

<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">No.</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Biodata</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key => $item)
        <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $item->nama }}</td>
            <td>{{ $item->umur }}</td>
            <td>{{ $item->bio }}</td>
            <td>
                <a href="/cast/{{$item -> id}}" class="mt-2 btn btn-info btn-sm">Info</a>
                <a href="/cast/{{$item -> id}}/edit" class="mt-2 btn btn-dark btn-sm">Edit</a>

                <form class="mt-2" action="/cast/{{$item -> id}}" method="POST">
                    @csrf
                    @method('delete')
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
    @empty
        <h1>Data Tidak Ada</h1>
    @endforelse
  </tbody>
</table>

@endsection