@extends('layout.theme')
    
@section('title')
    Form Registrasi Aktor
@endsection

@section('content')

<form method="POST" action="/cast">
    @csrf
    <div class="form-group">
            <label>Nama Aktor</label>
            <input type="text" name="nama" class="form-control">
    </div>

    @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
            <label>Umur</label>
            <input type="number" name="umur" class="form-control">
    </div>

    @error('umur')
            <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
            <label>Biodata</label>
            <textarea name="bio" cols="30" rows="10" class="form-control"></textarea>
    </div>

    @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-secondary">Create</button>
</form>

@endsection