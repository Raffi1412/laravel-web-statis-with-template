@extends('layout.theme')
@section('title')
Halaman Form
@endsection
@section('content')
<h1>Buat Account Baru</h1>
    <h3>Sing Up From</h3>
    <form action="/kirim" method="POST">
        @csrf
        <label>First Name :</labe> <br>
        <input type="text" name="first_name"> <br><br>

        <label>Last Name :</labe> <br>
        <input type="text" name="last_name"> <br><br>

        <label>Gender :</labe> <br>
        <input type="radio" name="gender" value="male">Male<br>
        <input type="radio" name="gender" value="female">Female<br>
        <input type="radio" name="gender" value="other">Other<br><br> 

        <label>Nationality: </label> <br>
        <select name="countries">
            <option value="ina">Indonesia</option>  
            <option value="rus">Rusia</option> 
            <option value="sau">Sauidi Arabia</option> 
            <option value= "usa">Amerika</option>
        </select> <br> <br>

        <label>Lenguage Spoken</label><br> 
        <input type="checkbox" name="language">Bahasa Indonesia<br> 
        <input type="checkbox" name="language">English<br>
        <input type="checkbox" name="language">Other<br><br>

        <label>Bio</label> <br>
        <textarea name="message" rows="10" cols="30"></textarea> <br> <br>

        <input type="submit" value="Sing Up">
    
</form>
@endsection
    