<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/register', 'AuthController@reg');
Route::post('/kirim', 'AuthController@kirim');
Route::get('/data-tables', 'IndexController@table');

// CRUD Cast

// CREAT
// Menunju form pendaftaran aktor
Route::get('/cast/create', 'CastController@create');
//Menyimpan data form ke database tabel cast
Route::post('/cast', 'CastController@store');

// READ
// Ambil data ke database dan ditampilkan ke blade
Route::get('/cast', 'CastController@index');
// route detail aktor
Route::get('/cast/{cast_id}', 'CastController@show');

// Update
// Route untuk mengarah ke form edit
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
// Route untuk mengarah ke form edit
Route::put('/cast/{cast_id}', 'CastController@update');

// Delete
// Route delete data berdasarkan id
Route::delete('/cast/{cast_id}', 'CastController@destroy');
