<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function reg() {
        return view('page.register');
    }

    public function kirim(Request $request){
        $firstname = $request['first_name'];
        $lastname = $request['last_name'];
        
        return view('page.welcome', compact("firstname", "lastname"));
    }
}
